from search import *
from P1 import *
from P2 import *

def main():
    # ______________________________________________________________________________
    
    print("\n   ~ The Water Jug Problem ~   \n")
    
    # Input data
    jug1_initial = 0; jug2_initial = 0
    jug1_max_capacity = 4; jug2_max_capacity = 3
    jug1_expected = 2; jug2_expected = 0
    
    print(" The dimensions of the jugs:", jug1_max_capacity ,"&", jug2_max_capacity, "gallons.")
    print(" Needed state:", jug1_expected,"in jug_1  & ",jug2_expected,"in jug_2.")
    
    # Problem initialization
    problem1 = WaterJug((jug1_initial, jug2_initial), (jug1_expected, jug2_expected), (jug1_max_capacity, jug2_max_capacity))

    # UNINFORMED Search using BFS on Graph
    solution1_P1 = breadth_first_graph_search(problem1).solution()
    print("\nSolution:", solution1_P1)
    
    # INFORMED Search using A*
    solution2_P1 = astar_search(problem1).solution()
    print("\nSolution:", solution2_P1, '\n')
    
    # ______________________________________________________________________________

    print("\n\n   ~ The 15-puzzle problem ~   \n")

    # Input data
    initial_state = (2, 0, 3, 4, 1, 6, 7, 8, 5, 14, 10, 12, 9, 13, 11, 15)
    goal_state = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0)

    problem2_1 = FifteenPuzzleEuclidean(initial_state, goal_state)
    solution1_P2 = astar_search(problem2_1).solution()
    print("Solution using Euclidean heuristic function:\n", solution1_P2, '\n')

    problem2_2 = FifteenPuzzleMissingTilesCR(initial_state, goal_state)
    solution2_P2 = astar_search(problem2_2).solution()
    print("Solution using number of missing tiles out of column/row heuristic function:\n", solution2_P2, '\n')
    
    # ______________________________________________________________________________

if __name__ == "__main__":
    main()