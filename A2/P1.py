from search import Problem

class WaterJug(Problem):
    
    def __init__(self, initial, goal, max_quantity):
        self.max_quantity = max_quantity
        Problem.__init__(self, initial, goal)
    
    def actions(self, curr_state):
        possible_actions = []
        
        # Fill First
        possible_actions.append( (self.max_quantity[0], curr_state[1]) )
        
        # Fill Second
        possible_actions.append( (curr_state[0], self.max_quantity[1]) )

        # Empty First
        possible_actions.append( (0, curr_state[1]) )
        
        # Empty Second
        possible_actions.append( (curr_state[0], 0) )

        # Pour from First -> Second
        local_min = min(curr_state[0], (self.max_quantity[1] - curr_state[1]))
        possible_actions.append ( (curr_state[0] - local_min, curr_state[1] + local_min) )

        # Pour from Second -> First
        local_min =  min(curr_state[1], (self.max_quantity[0] - curr_state[0]))
        possible_actions.append ( (curr_state[0] + local_min, curr_state[1] - local_min) ) 

        return possible_actions

    def result(self, state, action):
        state = action
        return state

    def goal_test(self, state):

        return state == self.goal

    # Euclidian Distance
    def h(self, node):
        return sum( ( (s // 4 - g // 4) ** 2 + (s % 4 - g % 4) ** 2) ** 1/2 for (s, g) in zip(node.state, self.goal))