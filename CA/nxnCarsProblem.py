from search import *

def maxBase5(n):

    """ Returns a number which represents the maximum number which
    can be represented in base 5 with n digits """
    
    maxx = 0
    for i in range(n):
        maxx += 4 * (5**i)
    return maxx

def toBase5(x,  n):
    """ Converts given decimal x to n-digit number in base 5 """
    
    digits = []
    while x > 0:
        digits.insert(0, x % 5)
        x  = x // 5

    length = len(digits)

    for _ in range(length, n):
        digits.insert(0, 0)
    
    return digits

class nxnCarsProblem(Problem):
    """ The problem of n cars moving on n x n grid 
    from the bottom row, reversed to the top row"""

    iterations = 0
    visited_states = set()

    def __init__(self, n):
        self.n = n
    
        initial = []
        goal = [] 

        for i in range(n):
            initial.append(i)
            
        for i in range(n * n - 1, n * (n - 1) - 1, -1):
            goal.append(i)
    
        Problem.__init__(self, tuple(initial), tuple(goal))          
    
    def build_state(self, state, actions):
        built_state = list(state)

        # 0 - SIT, 1 - UP, 2 - DOWN, 3 - LEFT, 4 - RIGHT
        
        for i in range(len(actions)):
            if actions[i] == 0: # SIT
                continue

            elif actions[i] == 1: # UP
                if state[i] < self.n or built_state[i] - self.n in state:
                    return []
                else: built_state[i] -= self.n
            
            elif actions[i] == 2: # DOWN
                if state[i] >= self. n * (self.n - 1) or built_state[i] + self.n in state: 
                    return []
                else: built_state[i] += self.n
            
            elif actions[i] == 3: # LEFT
                if state[i] % self.n == 0 or built_state[i] - 1 in state:
                    return []
                else: built_state[i] -= 1
            
            elif actions[i] == 4: # RIGHT
                if state[i] % self.n == self.n - 1 or built_state[i] + 1 in state:
                    return []
                else: built_state[i] += 1

        return tuple(built_state)

    def is_valid(self, state, actions):

        built_state = self.build_state(state, actions)
        
        # check for available build
        if len(built_state) == 0:
            return False

        # check for collisions
        if len(built_state) != len(set(built_state)):
            return False
        
        # check if it was visited
        if built_state in self.visited_states:
            return False
        else: 
            self.visited_states.add(built_state)

        return built_state
  


    def actions(self, state):
        all_possible_actions = []

        max_actions = maxBase5(self.n)

        for i in range(max_actions + 1):
            possible_state_coded = toBase5(i, self.n)
            possible_state = self.is_valid(state, possible_state_coded)          
            if possible_state != False:
                all_possible_actions.append(possible_state)
        self.iterations += len(all_possible_actions)
        
        print(self.iterations)
        
        return all_possible_actions
    
    def result(self, state, action):
        """ Given state and action, return a new state that is the result of the action.
        Action is assumed to be a valid action in the state """
        return tuple(action)
    
    def goal_test(self, state):
        """ Given a state, return True if state is a goal state or False, otherwise """
        return state == self.goal

    def h(self, node):
        """ Return the heuristic value for a given state. Default heuristic function used is
        h(n) = number of misplaced cars """

        return sum(s != g for (s, g) in zip(node.state, self.goal))