from search import *
import search
import string
import nxnCarsProblem

def main():
	print(" n =", end = " ")
    car = nxnCarsProblem.nxnCarsProblem(int(input()))
    src = search.astar_search(car, car.h)
    print(src.solution())
   
main()