#include <iostream>

using namespace std;

int main()
{
    string name, surname;
    cout << "Name : "; cin >> name;
    cout << "Surname : "; cin >> surname;

    int sum = 0;

    for(auto it : name)
        sum += it;

    for(auto it : surname)
        sum += it;

    cout << sum % 4 + 1 << endl;

    system("pause");
}
