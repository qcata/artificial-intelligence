
/**
 * Prob1
 * through_train(+Start: variable, ?End: variable).
 * 
 * Returns true if a travel is possible between two cities through an intermediate city.
 * 
 * @param Start The city of departure.
 * @param End The city of arrival.
 *  
 * Examples:
 * 
 * ?- through_train(sibiu, bucharest).
 * true
 * 
 * ?- through_train(constanta, craiova).
 * true
 * 
 * ?- through_train(craiova, bucharest).
 * false
 */

direct_train(craiova, bucharest).
direct_train(sibiu, craiova).
direct_train(deva, sibiu).
direct_train(brasov, deva).
direct_train(pitesti, brasov).
direct_train(ploiesti, pitesti).
direct_train(constanta, ploiesti).

through_train(Start,End):-( direct_train(Start,Aux),direct_train(Aux,End)).
through_train(Start,End):-( direct_train(Aux,End)-> through_train(Start,Aux)).



/**
 * Prob2
 * translate_list(+List1: list, -List2: list).
 * 
 * Translates a list of roumanian words in an list of english words.
 * 
 * @param List1 List of roumanian words.
 * @param List2 List of english words.
 *  
 * Examples:
 * 
 * ?- translate_list([unu, doi, trei], List2).
 * List2 = [one, two, three]
 * 
 * ?- translate_list([noua, doi, patru, cinci], List2).
 * List2 = [nine, two, four, five]
 * 
 */

translate(unu, one).
translate(doi, two).
translate(trei, three).
translate(patru, four).
translate(cinci, five).
translate(sase, six).
translate(sapte, seven).
translate(opt, eight).
translate(noua, nine).

translate_list([], []).
translate_list([H|T], [NewH|NewT]):- 
    translate(H, NewH),
    translate_list(T,NewT). 


/**
 * Prob3
 * duplicate(+List1 : list, +N : variable, ?List2 : list).
 *
 * Duplicates each element of the list List1 N times in the list List2.
 *
 * @param List1 is the input list
 * @param N is the number of copies
 * @param List2 is the output list
 *
 * Examples:
 * 
 * ?- duplicate([1, 2, 3], 2, List2).	
 * List2 = [1, 1, 2, 2, 3, 3]
 * 
 * ?- duplicate([6, 9], 3, List2).
 * List2 = [6, 6, 6, 9, 9, 9]
 */

duplicate(List1,N,List2) :- duplicate(List1,N,List2,N).
duplicate([],_,[],_).
duplicate([_|T],N,List2,0) :- duplicate(T,N,List2,N).
duplicate([H|T],N,[H|List2],Cnt) :- 
    Cnt > 0, 
    NCnt is Cnt - 1, 
    duplicate([H|T],N,List2,NCnt).



/**
 * Prob4
 * insert_element(+List: List, +Element: Variable, +Position: Variable, ?Result: List).
 * 
 * Inserts a given element at a given position into a list.
 * 
 * @param List is the list in which we insert the element
 * @param Element is the element we want to insert
 * @param Position is the position on which we want to insert the element
 * @param Result is the resulting list with the element inserted
 * 
 * Examples:
 * 
 * ?-insert_element([1, 2, 4, 5], 77, 3, Result).
 * Result = [1, 2, 77, 4, 5]
 * 
 * ?-insert_element([9, 2, 1, 7, 4], 77, 1, Result).
 * Result = [77, 9, 2, 1, 7, 4]
 */
insert_element(List, Element, 1, [Element|List]).
insert_element([HList|TList], Element, Position, [HList|Result]):- 
    CurrentPosition is Position - 1,
    insert_element(TList, Element,  CurrentPosition, Result).



/**
 * Prob5
 * range(+Left: Variable, +Right: Variable, ?Result: List).
 * 
 * Generates a list with all consecutive integers within a given range.
 * 
 * @param Left is the first element of the list.
 * @param Right is the last element of the list.
 * @param Result is the resulting list.
 * 
 * Examples: 
 * 
 * ?-range([1, 2, 4, 5], 77, 3, Result).
 * Result = [1, 2, 77, 4, 5]
 * 
 * ?-range([9, 2, 1, 7, 4], 77, 1, Result).
 * Result = [77, 9, 2, 1, 7, 4]
 */
range(Left,Left,[Left]).
range(Left,Right,[Left|AuxList]) :- 
    Left < Right,
    Iterator is Left + 1, 
    range(Iterator, Right, AuxList).